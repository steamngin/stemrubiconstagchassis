from steamtools.lib.dal.access.structured import SteamDataAccessLib
from steamtools.api.etl.base.engine import BaseEngine


class DemoEngine(BaseEngine):
    @property
    def _module(self):
        return __file__

    def _transform_data(self, dataframe, location):
        dataframe.loc[0, ['location']] = [location]

        return dataframe

    def extract_dataframe(self, t, **params):
        self.logger.debug('extract params: {}'.format(params))

        sql = params['extract_sql_statement']

        extracted = SteamDataAccessLib.call_sql(t.st001,
                                                self.logger,
                                                sql,
                                                is_select=True)

        self.logger.debug('extracted dataframe:\n{}'.format(extracted))

        return extracted

    def transform_dataframe(self, extracted, **params):
        self.logger.debug('transform params: {}'.format(params))

        location = params['updated_location']

        transformed = self._transform_data(extracted, location)

        self.logger.debug('transformed dataframe:\n{}'.format(transformed))

        return transformed

    def load_dataframe(self, t, transformed, **params):
        self.logger.debug('load params: {}'.format(params))

        table = params['load_to_table']

        SteamDataAccessLib.persist_dataframe(t.st001,
                                             transformed,
                                             table,
                                             if_exists='replace')
