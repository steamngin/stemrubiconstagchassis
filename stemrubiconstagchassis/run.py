from steamtools.api.etl.player import Player
from steamtools.api.etl.runner import Runner


def exec_sample_player():
    Player('player001', __file__, execution_instance_id='001').play()


def exec_sample_runner():
    extract_params = {
        'extract_sql_statement': 'select * from Pet'
    }

    Runner('runner001', __file__, execution_instance_id='001').run(
        extract_params=extract_params)


def main():
    exec_sample_runner()
    print('*****************************************************************')
    exec_sample_player()


main()
